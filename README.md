This is a project to gather my written macros for Autocad, Catia and SolidWorks.


Some files have Turkish comments in them. Please keep this in mind.


# Table of Contents

- [Excel](#excel)
- [SolidWorks](#solidworks)
- [Catia](#catia)
- [Autocad](#autocad)
- [Macro excerpts](#macro-excerpts)


### Excel

* [Push data from Excel to create part with assigned dimension values using SolidWorks or Catia](/source/excel_push-data-create-part-to-solidworks-or-catia.txt)


### SolidWorks

* [Get first level component count](/source/solidworks_01_get-first-level-comp-count.txt)
* [Create assy from folder](/source/solidworks_02_create-assy-from-folder.txt)
* [Create assy from part](/source/solidworks_03_create-assy-from-part.txt)
* [Create rectangle to new part](/source/solidworks_04_create-rectangle-to-new-part.txt)
* [Remove all equations from self and all child components](/source/solidworks_05_remove-all-equation-self-to-all-child.txt)
* [Create drawing and add sheet to every part from assembly](/source/solidworks_06_drawing-from-assembly-to-each-sheet-for-every-part.txt)
* [Convert drawings to pdf from specific folder](/source/solidworks_07_drawing-convert-to-pdf-from-folder.txt)
* [Create detail view and change scale](/source/solidworks_08_create-detail-view-change-scale.txt)
* [Export all configurations from part](/source/solidworks_09_export-all-configurations.txt)
* [Selects top component](/source/solidworks_10_select-top-component.txt)
* [Add a prefix to custom properties](/source/solidworks_11_add-prefix-to-custom-property.txt)
* [Rotate part in model view](/source/solidworks_12_rotate-part.txt)
* [Open step files from specific folder and save as SolidWorks format](/source/solidworks_14_open-step-from-folder-save-as-solidworks.txt)
* [Macro feature embed to a drawing - Part 1 - Adds a signature jpg file using prompt custom property value](/source/solidworks_15_part-1_feature-module_macro-feature-drawing-add-signature.txt)
* [Macro feature embed to a drawing - Part 2 - Adds a signature jpg file using prompt custom property value](/source/solidworks_15_part-2_regen-post-notify_macro-feature-drawing-add-signature.txt)
* [Macro feature embed to a drawing - Part 3 - Adds a signature jpg file using prompt custom property value](/source/solidworks_15_part-3_event-handler_macro-feature-drawing-add-signature.txt)
* [Sheet rename in a drawing](/source/solidworks_16_sheet-rename.txt)
* [Copy sheets from one drawing to another drawing back to back](/source/solidworks_17_copy-sheets-one-from-another.txt)


### Catia

* [Replaces text in all open drawing documents and sheets](/source/catia_01_opened-drawing-sheets-replace-text.txt)
* [Saves opened drawing docs to specified folder as pdf](/source/catia_02_opened-drawings-save-as-pdf.txt)
* [Reassign part numbers at top level without recurring](/source/catia_03_reassign-part-names-top-level-without-recurring.txt)
* [Replace text in current drawing](/source/catia_04_replace-text-open-drawing-first-sheet_sorunlu.txt)
* [Txt file reading](/source/catia_05_txt-read.txt)
* [Iterate through folder and do any event](/source/catia_06_scripting-iterate-folder-do-event.txt)
* [Get picture from views in a drawing](/source/catia_07_get-picture-drawing-views.txt)
* [Get user credentials](/source/catia_08_user-credentials.txt)
* [Replace and move text position in all open drawings and save as pdf](/source/catia_09_replace-move-text-open-drawings-all-folder-save-as-pdf.txt)
* [Count sheets in open drawings](/source/catia_10_opened-drawings-sheets-count.txt)
* [Open drawings in specific folder and save as pdf and remove detail sheets](/source/catia_11_folder-open-drawings-save-as-pdf-remove-unnecessary-detail-sheets.txt)
* [Open product and save as component to update links](/source/catia_12_open-part-product-save-as-update-links.txt)
* [Gets specific values and create txt file](/source/catia_13_gets-specific-values-writes-to-txt-file.txt)
* [Change parts dimensions, inject value](/source/catia_14_change-parts-feature-dimensions.txt)
* [Send message with MsgBox if text has balloon](/source/catia_15_msg-if-text-with-balloon.txt)
* [Excel vlookup and replace item numbers in all sheets](/source/catia_16_vlookup-excel-replace-item-number-drawing-sheets-balloons.txt)
* [Excel vlookup and replace text](/source/catia_17_replace-text-excel-vlookup.txt)

### Autocad

* [Creates layouts and setups print settings for A3 sized sheet blocks by window selection. Arranges them by cartesian positions](/source/autocad_pdf-order-files.txt)

### Macro excerpts

* [Autocad excerpts](/source/Macro-bölükleri-autocad.txt)
* [Catia excerpts](/source/Macro-bölükleri-catia.txt)
* [SolidWorks excerpts](/source/Macro-bölükleri-sw.txt)
* [Excel excerpts](/source/Macro-bölükleri-excel.txt)
