Option Explicit
Dim swApp As SldWorks.SldWorks
Dim fileerror As Long
Dim filewarning As Long
Dim boolstatus As Long
Dim model As ModelDoc2
Dim draw As DrawingDoc
Dim sheetnames As Variant
Dim shcount As Integer
Dim sh As Sheet
Dim countfordelete As Integer
Dim subdraw As Variant
Dim i As Variant

Sub main()

Set swApp = Application.SldWorks
countfordelete = 0
Dim folderset As Object
Dim folderpath As Object
Set folderset = CreateObject("Scripting.FileSystemObject")
Set folderpath = folderset.GetFolder("D:\Extension\computer aided design\macro\copy-sheet-from-one-another\")

For Each subdraw In folderpath.files
If Left(subdraw.Name, 15) = "Draw-test-copy1" Then
swApp.OpenDoc6 subdraw.Path, swDocDRAWING, swOpenDocOptions_Silent, "", fileerror, filewarning
Set model = Nothing
swApp.OpenDoc6 "D:\Extension\computer aided design\macro\copy-sheet-from-one-another\Draw-test-copy2.SLDDRW", swDocDRAWING, swOpenDocOptions_Silent, "", fileerror, filewarning
eventhappens subdraw.Name
End If
Next

End Sub

Function eventhappens(opendraw As String)

swApp.ActivateDoc opendraw
Set model = swApp.ActiveDoc
Set draw = model
shcount = draw.GetSheetCount

For i = 0 To shcount - 1

sheetnames = draw.GetSheetNames
boolstatus = draw.Extension.SelectByID2(sheetnames(i), "SHEET", 0, 0, 0, False, 0, Nothing, 0)
model.EditCopy

swApp.ActivateDoc Left(opendraw, 14) + "1.SLDDRW"
If countfordelete > 0 Then GoTo dontwaitforme
countfordelete = countfordelete + 1
deletesheets Left(opendraw, 14) + "1.SLDDRW"
dontwaitforme:
draw.ActivateSheet "sheet3"
boolstatus = draw.Extension.SelectByID2("Sheet331", "SHEET", 0, 0, 0, False, 0, Nothing, 0)
boolstatus = draw.PasteSheet(swInsertOption_BeforeSelectedSheet, swRenameOption_No)
Next


End Function

Function deletesheets(drawname As String)

If Len(drawname) > 24 Then Exit Function
swApp.ActivateDoc Left(drawname, 14) + "1.SLDDRW"
Set model = swApp.ActiveDoc
Set draw = model
shcount = draw.GetSheetCount
sheetnames = draw.GetSheetNames

For i = 3 To shcount - 1
draw.ActivateSheet sheetnames(i)
boolstatus = draw.Extension.SelectByID2(sheetnames(i), "SHEET", 0, 0, 0, False, 0, Nothing, 0)
model.DeleteSelection False
Next

End Function
