'Create rectangle to new part

Dim swApp As SldWorks.SldWorks


Sub main()

Set swApp = Application.SldWorks

Dim part As SldWorks.ModelDoc2
Dim boostatus As Boolean
Dim longWarnings As Long
Dim longstatus As Long
Dim swSM As SldWorks.SketchManager
Dim swSegment As SldWorks.SketchSegment

Dim temp
Dim activetitle
temp = swApp.GetUserPreferenceStringValue(swDefaultTemplatePart)


Set part = swApp.NewDocument(temp, 0, 0, 0)
activetitle = part.GetTitle
swApp.ActivateDoc2 activetitle, False, longstatus
Set part = swApp.ActiveDoc

Dim X1 As Double
Dim Y1 As Double
Dim Z1 As Double
Dim X2 As Double
Dim Y2 As Double
Dim Z2 As Double
X1 = 0
Y1 = 0
Z1 = 0
X2 = 1
Y2 = 1
Z2 = 0

boolstatus = part.SelectByID("FRONT PLANE", "Plane", 0, 0, 0)
part.SketchManager.InsertSketch True
Set swSM = part.SketchManager
swSM.CreateCenterRectangle X1, Y1, Z1, X2, Y2, Z2
'Set swSegment =
part.ViewZoomtofit2

End Sub
